//Edris Zoghlami
//1935687


package lab02.eclipse;

public class BikeStore {

	
	public static void main(String[] args) {
		Bicycle bikes[] = new Bicycle[4];
		bikes[0] = new Bicycle("honda",5,45);
		bikes[1] = new Bicycle("toyota",6,52);
		bikes[2] = new Bicycle("lamborgini",10,75);
		bikes[3] = new Bicycle("bugatti",15,86);
		
		for(int i=0;i<4;i++) {
			System.out.println(bikes[i].toString());
		}
		
	}
}
